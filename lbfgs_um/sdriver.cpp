#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

extern "C"{
 void lbfgs_(int *N,int *M,float *X,float *F,float *G,int *DIAGCO,float *DIAG,int IPRINT[],float *EPS,float *XTOL,float *W,int *IFLAG);  
}

struct{
 int LP,MP;
 float GTOL,STPMIN,STPMAX;
} LB2;

int main(){
 //N is number of unknowns
 //M is number of remembered gradients used to build approximated Hessian
 int N=100,M=5,IPRINT[2]={1,0},DIAGCO=0,ICALL=0,IFLAG=0;
 float EPS=1e-5,XTOL=1e-16,F;
 float *X=new float[N]();
 float *G=new float[N]();
 float *DIAG=new float[N]();
 float *W=new float[N*(2*M+1)+2*M]();
 
 //X contains initial variables and will be updated during iteration
// for(int i=0;i<N;i+=2){
//  X[i]=-1.2;
//  X[i+1]=1.;
// }
 
 while(true){
  //user supplied objective function and gradient evaluation function
  F=0.;
  for(int i=0;i<N;i+=2){
   float T1=1.-X[i]; 
   float T2=1e1*(X[i+1]-X[i]*X[i]);
   G[i+1]=2e1*T2;
   G[i]=-2*(X[i]*G[i+1]+T1);
   F+=T1*T1+T2*T2;
  }
   
  //call solver
  lbfgs_(&N,&M,X,&F,G,&DIAGCO,DIAG,IPRINT,&EPS,&XTOL,W,&IFLAG);

  ICALL++;
  if(IFLAG<=0 || ICALL>2000) break;
 }

 for(int i=0;i<N;i+=5) fprintf(stderr,"%.10f %.10f %.10f %.10f %.10f\n",X[i],X[i+1],X[i+2],X[i+3],X[i+4]);

 delete []X;delete []G;delete []DIAG;delete []W;
 return 0;
}
