#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>

#include "lbfgs.h"

using namespace std;

float camel(float *x,float *g){
    float x02=x[0]*x[0];
    float x04=x02*x02;
    float x12=x[1]*x[1];
    float f=(4.f-2.1*x02+x04/3.f)*x02+x[0]*x[1]+4*(x12-1)*x12;
    g[0]=8*x[0]-8.2*x[0]*x02+2*x[0]*x04+x[1];
    g[1]=x[0]+16*x[1]*x12-8*x[1];
    return f;
}

int main(int argc,char **argv){
 //N is number of unknowns
 //M is number of remembered gradients used to build approximated Hessian
 int N=2,M=5,DIAGCO=0,ICALL=0,IFLAG=0;
 float F;
 float *X=new float[N]();
 X[0]=3.f;X[1]=2.f;
 float *G=new float[N]();
 float *DIAG=new float[N]();
 float *W=new float[N*(2*M+1)+2*M]();
 int *isave=new int[nisave]();
 float *dsave=new float[ndsave]();
 
 while(true){
  //user supplied objective function and gradient evaluation function
  F=camel(X,G);

  //call solver
  lbfgs(N,M,X,F,G,DIAGCO,DIAG,W,IFLAG,isave,dsave);

  ICALL++;
  if(IFLAG<=0 || ICALL>2000) break;
 }

 cout<<X[0]<<" "<<X[1]<<endl;
// for(int i=0;i<N;i+=5) fprintf(stderr,"%.10f %.10f %.10f %.10f %.10f\n",X[i],X[i+1],X[i+2],X[i+3],X[i+4]);

 delete []X;delete []G;delete []DIAG;delete []W;
 delete []isave;delete []dsave;
 return 0;
}
